const WXAPI = require('apifm-wxapi')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    newsList: undefined,
    content: '...',
    relation: '', // 初始化关系文案
    range :["爸爸", "妈妈", "爷爷", "奶奶", "外公", "外婆", "儿子", "女儿", "兄弟", "姐妹"],
    dialogVisible: false, // 控制对话框的显示与隐藏
    isRecording: false ,
    speeking: false ,
    voiceFileName: '' ,
    selectedImagePath: '/images/icon_default.png' ,
    hasChooseIcon: false,
    description: '' ,
    showTextInputDialog: false,
    roleName:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: wx.getStorageSync('mallName')
    })
  },

  onInput: function(event) {
    console.log(event.detail.value); // 打印用户输入的文本
    // 如果你想将输入的文本保存到状态变量中，可以使用 this.setData() 方法进行设置
    this.setData({ roleName: event.detail.value });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  goToNewPerson: function() {
    var params = {
      relation: this.data.relation,
      roleName: this.data.roleName,
      voiceFile:this.data.voiceFileName,
      selectedImagePath:this.data.selectedImagePath,
      description:this.data.description,
    };

    var queryParams = Object.keys(params).map(function(key) {
      return key + '=' + encodeURIComponent(params[key]);
    }).join('&');
  
    wx.navigateTo({
      url: '/pages/news-detail/main?' + queryParams
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.fetchNews()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  fetchNews() {
    WXAPI.cmsArticles({
      categoryId: wx.getStorageSync('news_category_zs').id
    }).then(res => {
      if (res.code === 0) {
        this.setData({
          newsList: res.data
        })
      } else {
        this.setData({
          newsList: []
        })
      }
    })
  },
  goDetail(e) {
    wx.navigateTo({
      url: '/pages/news-detail/main?newsId=' + e.currentTarget.dataset.id
    })
  },

  pickerChange: function(e) {
    var value = e.detail.value; // 获取选择器选择的索引值
    var range = this.data.range; // 获取选择器的范围数组
    var valueString = range[value]; // 获取选中的关系文案
    console.log("-----我是日志输出-----" + valueString)
    this.setData({
      relation: valueString // 将选中的关系文案填充到关系后面
    });
  },

  showDialog: function() {
    this.setData({
      dialogVisible: true // 显示对话框
    });
  },

  hideDialog: function() {
    this.setData({
      dialogVisible: false // 隐藏对话框
    });
  },

  startRecord: function() {
    var global = this
    this.setData({
      isRecording: true // 当开始录制时将 isRecording 设置为 true，显示 <image> 元素
    });
    wx.getSetting({
      success(res) {
        if (!res.authSetting['scope.record']) {
          wx.authorize({
            scope: 'scope.record',
            success() {
              // 用户已经授权，可以开始录制
              console.log("-----用户已经授权，可以开始录制-----")
              
          console.log("----本来就有权限-----") 
          // 用户已经授权，可以开始录制
          let recorderManager = wx.getRecorderManager();
          recorderManager.onStart(() => {
          console.log('--real--开始录音');
          });
          recorderManager.onStop((res) => {
            console.log('--real--结束录音', res.tempFilePath);
            //-------------------录音结束-----------------------
            global.setData({
              isRecording: false,
              speeking:true ,
              voiceFileName:res.tempFilePath
            });
            wx.showToast({
              title: "录音成功，开始播放",
              icon: 'none'
            })
            var audioContext = wx.createInnerAudioContext();

            audioContext.src = res.tempFilePath;
          
            // 监听音频播放事件
            audioContext.onPlay(function() {
              console.log('音频播放开始');
            });
            
            // 监听音频暂停事件
            audioContext.onPause(function() {
              console.log('音频播放暂停');
            });
            
            // 监听音频停止事件
            audioContext.onStop(function() {
              console.log('音频播放停止');
            });
            
            // 监听音频播放完成事件
            audioContext.onEnded(function() {
              console.log('音频播放结束');

              global.setData({
                isRecording: false,
                speeking:false ,
                dialogVisible:false
              });

              wx.showToast({
                title: "播放完毕"
              })
            });
            
            // 监听音频错误事件
            audioContext.onError(function(errMsg) {
              console.log('音频播放错误---1：', errMsg);
            });
            
            // 播放音频
            audioContext.play();
          });
          recorderManager.onError((res) => {
            console.log('--real--录音错误', res.errMsg);
            global.setData({
              dialogVisible: false, // 当开始录制时将 isRecording 设置为 true，显示 <image> 元素
              isRecording: false 
            });
            wx.showToast({
              title: res.errMsg,
              icon: 'none'
            })
          });

          recorderManager.start({
            duration: 3000, // 录音最长时长，单位：毫秒
            format: 'aac', // 录音的格式，可以是'mp3'或'aac'等
          });        
        
            },
            fail() {
              // 用户拒绝授权，无法录制
              wx.showToast({
                title: '授权失败',
                icon: 'none', 
              });
            },
          });
        } else {
          console.log("----本来就有权限-----") 
          // 用户已经授权，可以开始录制
          let recorderManager = wx.getRecorderManager();
          recorderManager.onStart(() => {
          console.log('--real--开始录音');
          });
          recorderManager.onStop((res) => {
            console.log('--real--结束录音', res.tempFilePath);
            //-------------------录音结束-----------------------
            global.setData({
              isRecording: false,
              speeking:true ,
              voiceFileName:res.tempFilePath
            });
            wx.showToast({
              title: "录音成功，开始播放",
              icon: 'none'
            })

            var audioContext = wx.createInnerAudioContext();

            audioContext.src = res.tempFilePath;
          
            // 监听音频播放事件
            audioContext.onPlay(function() {
              console.log('音频播放开始');
            });
            
            // 监听音频暂停事件
            audioContext.onPause(function() {
              console.log('音频播放暂停');
            });
            
            // 监听音频停止事件
            audioContext.onStop(function() {
              console.log('音频播放停止');
            });
            
            // 监听音频播放完成事件
            audioContext.onEnded(function() {
              console.log('音频播放结束');

              global.setData({
                isRecording: false,
                speeking:false ,
                dialogVisible:false
              });

              wx.showToast({
                title: "播放完毕"
              })
            });
            
            // 监听音频错误事件
            audioContext.onError(function(errMsg) {
              console.log('音频播放错误---1：', errMsg);
            });
            
            // 播放音频
            audioContext.play();

            // wx.downloadFile({
            //   url: res.tempFilePath,
            //   success: function(res) {
            //     var filePath = res.tempFilePath; // 下载的临时文件路径
            //     wx.saveFile({
            //       tempFilePath: filePath,
            //       success: function(res) {
            //         var savedFilePath = res.savedFilePath; // 保存的文件路径
            //         console.log('语音保存成功，保存路径为：' + savedFilePath);
            //       },
            //       fail: function(err) {
            //         console.log('语音保存失败：', err);
            //       }
            //     });
            //   },
            //   fail: function(err) {
            //     console.log('语音下载失败：', err);
            //   }
            // });


          });
          recorderManager.onError((res) => {
            console.log('--real--录音错误', res.errMsg);
            global.setData({
              dialogVisible: false, // 当开始录制时将 isRecording 设置为 true，显示 <image> 元素
              isRecording: false 
            });
            wx.showToast({
              title: res.errMsg,
              icon: 'none'
            })
          });

          recorderManager.start({
            duration: 3000, // 录音最长时长，单位：毫秒
            format: 'aac', // 录音的格式，可以是'mp3'或'aac'等
          });        
        }
      },
    });
  },

  chooseImage: function() {
    var that = this;
    // 在你的页面代码的某个方法中，或者在需要选择图片的地方
    wx.chooseImage({
      count: 1, // 最多可选择的图片数量，这里设置为1，也可以根据需要设置更多
      sizeType: ['original', 'compressed'], // 图片源格式，原图和压缩图都可选
      sourceType: ['album', 'camera'], // 图片的来源，相册和相机都可选
      success: function(res) {
        var tempFilePaths = res.tempFilePaths; // 选择的图片的临时文件路径
        // 可以在这里进行后续操作，如上传图片或显示选中的图片
        console.log('选择的图片路径：', tempFilePaths);
        that.setData({
          selectedImagePath: tempFilePaths[0],
          hasChooseIcon: true
        });
      },
      fail: function(err) {
        console.log('选择图片失败：', err);
      }
    });
  },

  showInputDialog: function () {
    this.setData({
      showTextInputDialog: true
    });
  },
  onCancelTextInputDialog: function() {
    this.setData({
      showTextInputDialog: false
    });
  },
  onConfirmTextInputDialog: function(event) {
    var inputText = event.detail.inputText;
    console.log('输入的内容:', inputText);
    this.setData({
      showTextInputDialog: false,
      description:inputText
    });
  },
})