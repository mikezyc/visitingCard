const WxParse = require('../../wxParse/wxParse.js');
const WXAPI = require('apifm-wxapi')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    newsId: undefined,
    inputValue:"",
    newsObject: {
      title: undefined,
      content: undefined
    },
    messages: [
      { sender: 'received', avatar: '/images/cart.png', content: '你好，欢迎光临1！你好，欢迎光临1你好，欢迎光临1你好，欢迎光临1你好，欢迎光临1你好，欢迎光临1' },
      { sender: 'sent', avatar: '/images/ico-add-addr.png', content: '你好!' },
      { sender: 'received', avatar: '/images/cart.png', content: '我想了解关于产品的信息3。' },
      { sender: 'sent', avatar: '/images/ico-add-addr.png', content: '好的，我可以帮您查询相关产品信息，请稍等片刻4。' },
      { sender: 'received', avatar: '/images/cart.png', content: '你好，欢迎光临5！' },
      { sender: 'sent', avatar: '/images/ico-add-addr.png', content: '你好，请问有什么需要帮助的吗6？' },
      { sender: 'received', avatar: '/images/cart.png', content: '我想了解关于产品的信息7。' },
      { sender: 'sent', avatar: '/images/ico-add-addr.png', content: '好的，我可以帮您查询相关产品信息，请稍等片刻8。' },
      { sender: 'received', avatar: '/images/cart.png', content: '你好，欢迎光临1！你好，欢迎光临1你好，欢迎光临1你好，欢迎光临1你好，欢迎光临1你好，欢迎光临1' },
      { sender: 'sent', avatar: '/images/ico-add-addr.png', content: '你好!' },
      { sender: 'received', avatar: '/images/cart.png', content: '我想了解关于产品的信息3。' },
      { sender: 'sent', avatar: '/images/ico-add-addr.png', content: '好的，我可以帮您查询相关产品信息，请稍等片刻4。' },
      { sender: 'received', avatar: '/images/cart.png', content: '你好，欢迎光临5！' },
      { sender: 'sent', avatar: '/images/ico-add-addr.png', content: '你好，请问有什么需要帮助的吗6？' },
      { sender: 'received', avatar: '/images/cart.png', content: '我想了解关于产品的信息7。' },
      { sender: 'sent', avatar: '/images/ico-add-addr.png', content: '好的，我可以帮您查询相关产品信息，请稍等片刻8。' },
      // 添加更多消息...
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var roleName = decodeURIComponent(options.roleName);
    var relation = decodeURIComponent(options.relation);
    var voiceFile = decodeURIComponent(options.voiceFile);
    var selectedImagePath = decodeURIComponent(options.selectedImagePath);
    var description = decodeURIComponent(options.description);
    console.log("voiceFile:" + voiceFile)
    console.log("selectedImagePath:" + selectedImagePath)   
     console.log("description:" + description)
    this.setData({
      newsId: options.newsId
    })
    this.fetchNewsContent()
      // 设置新页面的标题
    wx.setNavigationBarTitle({
      title: roleName + "【" + relation + "】"
    });
    this.setData({
      messages: JSON.parse(wx.getStorageSync('myStringKey'))
    })
    this.scrollToBottom();
  },

  scrollToBottom() {
    wx.pageScrollTo({
      scrollTop: 10000, // 设置一个足够大的值，确保滚动到底部
      duration: 300 // 滚动动画的时长，单位为毫秒
    });
  },

  handleSend() {
    const newMessage = {
      sender: 'sent',
      avatar: '/images/ico-add-addr.png',
      content: this.data.inputValue
    };
    this.data.messages.push(newMessage);
    this.setData({
      messages: this.data.messages,
      inputValue: ""
    }, () => {
      //在setData回调中执行页面滚动到底部的操作
      this.scrollToBottom();
    });
  },

    
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  handleInput() {
    this.setData({
      newsObject: res.data
    })
  },

  
  handleInput: function(event) {
    console.log(event.detail.value); // 打印用户输入的文本
    // 如果你想将输入的文本保存到状态变量中，可以使用 this.setData() 方法进行设置
    this.setData({ inputValue: event.detail.value });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    wx.switchTab({
      url: '/pages/card/main'
    });
    wx.setStorageSync('myStringKey', JSON.stringify(this.data.messages));
  },


  thisPersonToJson() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  fetchNewsContent() {
    WXAPI.cmsArticleDetail(this.data.newsId).then(res => {
      if (res.code === 0) {
        this.setData({
          newsObject: res.data
        })
        wx.setNavigationBarTitle({
          title: res.data.title + ' - ' + wx.getStorageSync('mallName')
        })
        WxParse.wxParse('article', 'html', res.data.content, this, 5);
      }
    })
  }
})