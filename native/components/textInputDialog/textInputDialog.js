// textInputDialog.js

Component({
  properties: {
    show: {
      type: Boolean,
      value: false
    }
  },
  data: {
    inputText: ''
  },
  methods: {
    onCancel: function() {
      this.triggerEvent("cancel");
    },
    onConfirm: function() {
      this.triggerEvent("confirm", { inputText: this.data.inputText });
    },
    onInput: function(event) {
      this.setData({
        inputText: event.detail.value
      });
    }
  }
});